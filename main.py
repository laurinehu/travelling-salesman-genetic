import sys, random, math
from operator import itemgetter
from collections import Counter

def get_city_data():
    return {
        'Nancy' :{
            'Paris':282,
            'Bordeaux':672,
            'Toulouse':674,
            'Orléans':329,
            'Dijon':173
            },
        'Paris' :{
            'Nancy':282,
            'Bordeaux':503,
            'Toulouse':592,
            'Orléans':112,
            'Dijon':264
            },
        'Bordeaux' :{
            'Nancy':672,
            'Paris':503,
            'Toulouse':212,
            'Orléans':393,
            'Dijon':516
            },
        'Toulouse' :{
            'Nancy':674,
            'Paris':592,
            'Bordeaux':212,
            'Orléans':482,
            'Dijon':501
            },
        'Orléans' :{
            'Nancy':329,
            'Paris':112,
            'Bordeaux':393,
            'Toulouse':482,
            'Dijon':244
            },
        'Dijon' :{
            'Nancy':173,
            'Paris':264,
            'Bordeaux':516,
            'Toulouse':501,
            'Orléans':244
            },
    }


city_data = get_city_data()

def fitness(data):
    """ fitness function """ 
    return 0


def generate_list(nb,data):
    """ generates a random list of chromosomes 
    
    chromosome : list of cities the traveller has to pass in """
    paths_list =[]

    for i in range(int(nb)):
        tmp = []
        cities_list = list(data.keys())
        for j in range(6):
            randnum = random.randint(0, len(cities_list)-1)
            tmp.append(cities_list[randnum])
            del cities_list[randnum]

        tmp.append(tmp[0])
        paths_list.append(tmp)

    return paths_list


def calculate_distance(data, path):
    """calculates the distance of path given a table of data and a specific path
    
    data : dictionnary
    path : list of cities"""
    distance = 0
    for i,e in enumerate(path):
        if i+1 < len(path):
            distance += data[path[i]][path[i+1]]

    return distance


def select_best(data):
    """ given a list of cities, find the 2 shortest paths """
    selected = []
    tmp = list(data)

    for i in range(4):
        mini = min(tmp, key=itemgetter(1))
        selected.append(mini[0])
        del tmp[tmp.index(mini)]

    return selected


def mutate(data):
    """ given a list of children, perform random mutation
    
    some cases are forbidden in respect to the rules we need
    to respect :
        * first and last city are the same 
        * every city is in the list
    """
    for child in data :
        # cond 1 
        # if the first city is not equal to the last,
        # replace last with first
        if child[0] != child[-1]:
            child[-1] = child[0]
        
        # if duplicate exist, we replace randomly
        # with by values that are not yet in the
        # list
        # remove last because same as 0
        if len(Counter(child[:-1])) != len(child[:-1]):

            # recover cities that are not in list
            cities_to_add = set(city_data.keys()) - set(child[:-1])
            cities_to_add = list(cities_to_add)
            # recover cities to remove
            cities_duplicated = [x for x, v in dict(Counter(child[:-1])).items() if v>1] 

            for key, value in dict(Counter(child[:-1])).items():
                if value != 1:
                    # recover indexs that have duplicate values
                    indexs = [i for i, x in enumerate(child) if x in cities_duplicated and i!=0 and i!= len(child)-1]
                    counter_du = dict(Counter(child[1:len(child)-2]))
                    for k,v in counter_du.items() :
                        if v>1:
                            i=child.index(k)
                            del indexs[indexs.index(i)]

                    # indexs = [child.index(x) for x in cities_duplicated]
                    # print(indexs)
                    # remove first and last because they should stay like that
                    # if 0 in indexs:
                    #     indexs.remove(0)
                    #     indexs.remove(6)
                    # while duplicates exist, replace with new cities
                    while len(indexs) != 0 and len(cities_to_add) != 0: 
                        child[indexs.pop(0)] = cities_to_add.pop(0)

        # Random mutation, swap 2 chromozome that are neither the first or the last
        rand_1 = random.randint(1,len(child)-2)
        rand_2 = random.randint(1,len(child)-2)
        while rand_1 == rand_2:
            rand_2 = random.randint(1, len(child) - 2)

        tmp = child[rand_1]
        child[rand_1] = child[rand_2]
        child[rand_2] = tmp

    return data


def crossover(p1,p2):
    """ crosses randomly a part of p1 with the same part in p2 """
    begin = random.randint(0, len(p1)-1)
    end = random.randint(begin, len(p1)-1)

    child1 = p1[:begin]+p2[begin:end+1]+p1[end+1:]
    child2 = p2[:begin]+p1[begin:end+1]+p2[end+1:]

    return [child1, child2]


def buildChild(p1,p2):
    """ given 2 parents, creates 2 children with crossover & mutation """
    child = crossover(p1,p2)
    child = mutate(child)
    return child

def start(nb):
    """ run algorithm """
    path_list = generate_list(nb, city_data)
    past_score = 0
    nb_score_increase = 0

    # Ending condition : if the sum of distances goes up 5 times in a row, we break the loop
    while nb_score_increase <5:
        distance_list = []
    
        # create list of distances
        for route in path_list :
            distance_list.append([route, calculate_distance(city_data, route)])

        selected_parents = select_best(distance_list)

        children = []

        # create children
        # we take 2 couples of parents
        # we create 2 children for each couple
        for i in range(int(len(selected_parents)/2)):
            for x in buildChild(selected_parents[i],selected_parents[i+1]):
                children.append(x)

        selected_list = list(children)

        # select maxi 4 bests chromosomes
        # in order to complete the list of chromosomes
        for i in range(math.ceil((float(nb)-len(selected_list))/2)):
            # we just take 4 parents
            if i >= 4:
                break
            selected_list.append(selected_parents[i])
            del path_list[path_list.index(selected_parents[i])]

        # complete the rest of chromosomes list
        # with new chromosomes
        for i in range(int(nb)-len(selected_list)):
            random_number = random.randint(0,len(path_list)-1)
            selected_list.append(path_list[random_number])
            del path_list[random_number]

        sum_distance = 0

        # calculate total distance
        for item in selected_list:
            sum_distance += calculate_distance(city_data,item)

        path_list = list(selected_list)

        # Ending condition : if the sum of distances goes up 5 times in a row, we break the loop
        if sum_distance > past_score:
            nb_score_increase +=1
        else:
            nb_score_increase = 0

        past_score = sum_distance

    # print solution
    print("One of the best solution is this path : ")
    print(min([(x,calculate_distance(city_data,x)) for x in path_list], key=itemgetter(1))[0])
    print("it takes : ")
    print(min([(x,calculate_distance(city_data,x)) for x in path_list], key=itemgetter(1))[1], "kms")


def main():
    args = sys.argv[1:]

    if(len(args)>0):
        start(args[0])
    else:
        print('Param : nb of item')

if __name__ == '__main__':
  main()
